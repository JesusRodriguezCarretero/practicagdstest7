

public class CProductList extends CList {
	public CProduct SearchByName(String name) {
		CNode node=m_Start;
		while (node!=null) {
			if (((CProduct) node.m_Element).m_Name.equalsIgnoreCase(name)) {
				return (CProduct) node.m_Element;				
			}
			node=node.m_Next;
		}
		return null;
	}
	public CProduct SearchByCode(int code) {
		CNode node=m_Start;
		while (node!=null) {
			if (((CProduct) node.m_Element).m_Code==code) {
				return (CProduct) node.m_Element;
			}
			node=node.m_Next;
		}
		return null;
	}
	public void PushBack(CProduct e) {
		super.PushBack(e);
	}
	public void PrintProducts() {
        CNode node = m_Start;
	    System.out.println("LISTADO DE PRODUCTOS MUEBLES JOSE");	
		System.out.println("CODIGO PRODUCTO   NOMBRE    PRECIO PRODUCTO");	
        while (node != null) {
            CProduct product = (CProduct) node.m_Element;
            System.out.printf(Locale.ROOT,"%-18d%-20s%.1f\n", product.m_Code, product.m_Name, product.m_Price);		
            node = node.m_Next;	        
            }
	    }
}
